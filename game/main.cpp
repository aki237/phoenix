#include "engine.hpp"

int main(int argc, char *argv[]) {
  std::cout << "Welcome to phoenix engine!!" << std::endl;

  auto res = Phoenix::init();
  if (res.is_err()) {
    std::cout << res.err() << std::endl;
  }

  Phoenix::Engine engine("sample-game", "in.aki237.sample_game");
  engine.init(800, 600, "phoenix demo");
  engine.run();
  
  Phoenix::deinit();
  return 0;
}

