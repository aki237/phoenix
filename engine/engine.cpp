#include "engine.hpp"
#include "result.hpp"
#include <GL/gl.h>

namespace Phoenix {
  
  const char* get_glfw_error() {
    const char* error_string;
    glfwGetError(&error_string);
    return error_string;
  }
  
  Result<bool> init() {
    if (!glfwInit()) {
      return Result<bool>(get_glfw_error());
    }
    return Result<bool>(true);
  }

  void deinit() {
    glfwTerminate();
  }
  
  Engine::Engine(std::string _name, std::string _package_id) :
    name(_name),package_id(_package_id) {}

  Engine::~Engine() {
    glfwDestroyWindow(window);
  }
  
  Result<bool> Engine::init(int width, int height, std::string title) {
    window = glfwCreateWindow(width, height, title.c_str(), nullptr, nullptr);
    if (!window) {
      return Result<bool>(get_glfw_error());
    }
    glfwMakeContextCurrent(window);

    return Result<bool>(true);
  }

  void Engine::run() {
    glClearColor(0.2f, 0.2f, 0.2f, 1.0f);
    while (!glfwWindowShouldClose(window)) {
        glClear(GL_COLOR_BUFFER_BIT);
        glfwSwapBuffers(window);
        glfwPollEvents();
    }
  }
}
