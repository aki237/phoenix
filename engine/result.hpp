#pragma once
#include <iostream>

namespace Phoenix {
  template <typename T>
  class Result {
  private:
    T& result;
    std::string error;
    bool ok;
  public:
    Result(T);
    Result(std::string);

    bool is_ok();
    bool is_err();

    T& unwrap();
    std::string err();
  };


  template <typename T>
  Result<T>::Result(T x) : result(x) {
    ok = true;
  }

  template <typename T>
  Result<T>::Result(std::string err) {
    error = err;
    ok = false;
  }

  template <typename T>
  bool Result<T>::is_ok() {
    return ok;
  }

  template <typename T>
  bool Result<T>::is_err() {
    return !ok;
  }

  template <typename T>
  T& Result<T>::unwrap() {
    return result;
  }

  template <typename T>
  std::string Result<T>::err() {
    return error;
  }
}
