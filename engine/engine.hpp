#pragma once

#include <iostream>
#include <GLFW/glfw3.h>
#include "result.hpp"

namespace Phoenix {
  Result<bool> init();
  void deinit();
  
  class Engine {
  private:
    GLFWwindow* window;
    std::string name;
    std::string package_id;

  public:
    Engine(std::string name, std::string package_id);
    ~Engine();
    Result<bool> init(int, int, std::string);
    void run();
  };

}
