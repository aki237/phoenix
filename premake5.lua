workspace "phoenix"
    configurations { "debug", "release" }
    location "build"
    links {"glfw", "GL"}

project "engine"
    kind "StaticLib"
    language "C++"
    files { "engine/**.h", "engine/**.cpp" }
    includedirs {}

    filter { "configurations:debug" }
        defines { "DEBUG" }
        symbols "On"

    filter { "configurations:release" }
        defines { "NDEBUG" }
        optimize "On"

project "sample-game"
    kind "ConsoleApp"
    language "C++"
    files { "game/**.h", "game/**.cpp" }
    includedirs {
       "engine"
    }
    links {"engine"}


    filter { "configurations:debug" }
        defines { "DEBUG" }
        symbols "On"

    filter { "configurations:release" }
        defines { "NDEBUG" }
        optimize "On"
