# phoenix

WIP

Only tested to work in Linux (arch linux specifically.)
For other platforms and distros your mileage may vary.

Requires:
 - C/C++ Compiler toolchain (gcc/clang)
 - [premake5](https://premake.github.io/)
 - [GNU Make](https://www.gnu.org/software/make/)
 - [GLFW](https://www.glfw.org)
 - [drill](https://gitlab.com/aki237/drill)

Run the following to generate, compile and run the project.

```
drill b
```
